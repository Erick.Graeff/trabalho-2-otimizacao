#include <stdio.h>
#include <string.h>

#include "auxiliar.h"

int main(int argc, char *argv[]) {
    int corteViabilidade = 1,
        corteOtimalidade = 1,
        defaultFunction = 1;

    if(argc > 1) {
        for(int i = 1; i < argc; i++) {
            if(!strcmp(argv[i], "-f")) {
                corteViabilidade = 0;
                continue;
            }
            if(!strcmp(argv[i], "-o")) {
                corteOtimalidade = 0;
                continue;
            }
            if(!strcmp(argv[i], "-a")) {
                defaultFunction = 0;
                continue;
            }
        }
    }

    int numberOfHeroes, conflictNumber, afinityNumber;

    scanf("%d", &numberOfHeroes);

    int heroesArray[numberOfHeroes];
    for(int i = 0; i < numberOfHeroes; i++) {
        heroesArray[i] = i + 1;
    }

    scanf("%d", &conflictNumber);
    scanf("%d", &afinityNumber);

    Pair conflictArray[conflictNumber], afinityArray[afinityNumber];

    for(int i = 0; i < conflictNumber; i++) {
        scanf("%d", &conflictArray[i].x1);
        scanf("%d", &conflictArray[i].x2);
    }

    for(int i = 0; i < afinityNumber; i++) {
        scanf("%d", &afinityArray[i].x1);
        scanf("%d", &afinityArray[i].x2);
    }

    int group1[numberOfHeroes], group2[numberOfHeroes];
    for(int i = 0; i < numberOfHeroes; i++) {
        group1[i] = 0;
        group2[i] = 0;
    }

    // set groups
    for(int i = 0; i < numberOfHeroes; i++) {
        
    }

    return 0;
}